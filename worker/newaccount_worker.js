const queue=require('../config/kue');
const newAccountMailer=require('../mailer/newaccount_mailer');


queue.process("newAccount",function(job,done)
{
    //console.log("email workers is processing the job ",job.data);
    newAccountMailer.newAccount(job.data);
    done();
})