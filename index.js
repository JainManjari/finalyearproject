const express=require('express');
const env=require('./config/environment');
const sweetalert=require('sweetalert2');
const port=8000;
const app=express();
const expressLayouts=require('express-ejs-layouts');
app.use(expressLayouts);
const db=require('./config/mongoose');
console.log(env)
//for passing the data through form
app.use(express.urlencoded({extended:false}));



app.set("view engine","ejs");
app.set("views","./views");

app.use("/",require('./routes'));

app.listen(port,function(err)
{
    if(err)
    {
        console.log("error in starting the server ",err);
        return;
    }
    console.log("Server is working fine on port ",port);
})
