const Customer=require('../../../model/customer');

module.exports.index=async function(req,res)
{
    try
    {
        let customers=await Customer.find({}).sort("-createdAt");
        return res.json(200,{
            message:"Customer Database",
            customers:customers
        })
    }
    catch(err)
    {
        console.log("error ",err);
        return;
    }
}

module.exports.FoundByEmail=async function(req,res)
{
    try
    {
        let email=req.params.id;
        let customer=await Customer.findOne({email:email});
        if(customer)
        {
            return res.json(200,{
                message:"Customer Found!",
                customer:customer
            });
        }
        else
        {
            return res.json(404,{
                message:"No customer exists with this Email ID!"
            })
        }
    }
    catch(err)
    {
        console.log("error ",err);
        return;
    }

}