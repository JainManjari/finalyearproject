const Customer=require('../model/customer');
const queue=require('../config/kue');
const newaccountEmailWorker=require('../worker/newaccount_worker');

module.exports.home=function(req,res)
{
    return res.render("home",{
        title:"Database Server"
    });
}

module.exports.createCustomer=async function(req,res)
{
    try
    {
        //console.log(req.body);
        let customer=await Customer.findOne({email:req.body.email});
        if(!customer)
        {
            let customer=await Customer.create(req.body);
            console.log(customer);
            let job=queue.create("newAccount",customer).save(function(err)
            {
                if(err)
                {
                    //console.log("error in creating a queue ",err);
                    return;
                }
               // console.log("job enqueued ",job.id);
            });
            if(req.xhr)
            {
                return res.json(200,{
                    message:"Customer registered Successfully",
                    customer:customer
                });
            }
            return res.redirect("back");
            
        }
        else
        {
            if(req.xhr)
            {
                return res.json(200,{
                    message:"This Email Id already exists!"
                })
            }
            return res.redirect("/");
        }
    }
    catch(err)
    {
        console.log("error ",err);
    }
    
    
    //return res.redirect("back");
}