const mongoose=require('mongoose');
const env=require('./environment');

console.log(env.db)
 mongoose.connect(`mongodb://localhost/${env.db}`);
//dont use special characters in writing <password> use %123=>#
// const URL = `mongodb+srv://Manjari:2216Manjari%23@cluster0-5la9b.mongodb.net/${env.db}?retryWrites=true&w=majority`;

// mongoose.connect(URL,{
//     useUnifiedTopology:true,
//     useNewUrlParser:true
// });
const db=mongoose.connection;
db.on("error",console.error.bind("error in connecting with db"));
db.once("open",function()
{
    console.log("Successfully connected to db");
});
module.exports=db;
