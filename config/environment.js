const production=
{
    name:"production",
    db:process.env.MSBANK_DB,
    smtp:{
        service:'gmail',
        host:'smtp.gmail.com',
        port:587,
        secure:false,
        auth:{
            user:process.env.MSBANK_GMAIL_USERNAME,
            pass:process.env.MSBANK_GMAIL_PASSWORD
        }
    },
    emailAddress:process.env.MSBANK_GMAIL_USERNAME
}

module.exports=production;