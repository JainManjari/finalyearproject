const mongoose=require('mongoose');
require('mongoose-long')(mongoose);

const customerSchema=mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    phone:{
        type:String,
        required:true
    },
    account:{
        type:String,
        required:true
    },
    fingerprint:{
        //type:mongoose.Schema.Types.Long,
        type:String
    },
    balance:
    {
        type:String,
        required:true
    }

},{
    timestamps:true
});

const Customer=mongoose.model("Customer",customerSchema);
module.exports=Customer;
