const nodemailer=require('../config/nodemailer');
const env=require('../config/environment');

exports.newAccount=(newuser)=>{
    //console.log("inside new user ",newuser);
    let htmlString=nodemailer.renderTemplate({user:newuser},"/newaccount.ejs");

    nodemailer.transporter.sendMail({
        from:"manjarijain1998@gmail.com",
        to:newuser.email,
        subject:"Congratulations and Welcome to the Family!",
        html:htmlString
    },(err,info)=>{
        if(err)
        {
            console.log("Error in sending mail for new account ",err);
            return;
        }
       // console.log("Email sent: ",info);
        return;
    })
}