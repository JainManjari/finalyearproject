const express=require('express');
const router=express.Router();
const customerAPIController=require('../../../controllers/api/v1/customer_api');

router.get("/customer-database",customerAPIController.index);
router.get("/customer-database/info/:id",customerAPIController.FoundByEmail);

module.exports=router