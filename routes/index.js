const express=require('express');
const router=express.Router();
const homeController=require('../controllers/home_controller');
console.log("router loaded"); 

router.get("/",homeController.home);

//for using api
router.use("/api",require('./api/apiIndex'));

router.post("/create-customer",homeController.createCustomer);
module.exports=router;